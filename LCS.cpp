#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62

typedef long long ll;

using namespace std;
ll dp[101][101];
int main()
{
   // Test;
    ll t;
    cin>>t;
    while(t--)
    {
     ll n , m;
     cin>>n>>m;
     string s1 , s2;
     cin>>s1>>s2;
     Rep( i , n+1)
     {
        Rep( j ,  m +1)
        {
            if(i== 0 || j==0)
                dp[i][j]=0;
            else if(s1[i-1]== s2[j-1])
                dp[i][j]= 1 +dp[i-1][j-1];
            else
                dp[i][j]= max(dp[i-1][j] , dp[i][j-1]);
        }

     }
     cout<<dp[n][m]<<endl;
    }
}
